(** Player module *)

(* TODO: do we need name AND num ? *)
(** A player *)
type t = {num: int ; name: string}



class virtual player (a_name:string) (a_pen: Move.pos array) =
  object (_)
    val name = a_name
    val penguins = Array.copy a_pen
    val mutable next_move = (Move.W,0)
    val mutable score =0
    val mutable nb_turn_played = 0
    val mutable nb_teleport_used = 0
    method virtual play : unit
    method get_name = name
    method get_penguin_pos i = penguins.(i)
    method get_penguins_pos = penguins

    (* Here we verify that the penguin moved is effectively
     * owned by the player*)
    method move old_pos new_pos nb_t =
      nb_turn_played <- nb_turn_played+1;
      nb_teleport_used <- nb_teleport_used + nb_t;
      let found = ref false and i = ref 0 in
      while not !found && !i < Array.length penguins do
        if old_pos = penguins.(!i) then
          found := true;
        i := !i  + 1
      done;
      if not !found then
        failwith "You are trying to access to a penguin you do not own."
      else
        penguins.(!i - 1) <- new_pos

    method turn_counter = nb_turn_played
    method teleport_counter = nb_teleport_used
    method get_score = score
    method add_score n = 
      score <- score+n
    method virtual is_human : bool
    method virtual cpu_move : Move.elt Move.grid -> (Move.pos * Move.pos)
    method nb_penguin = Array.length penguins
  end

class humanPlayer (a_name:string) (a_pen:Move.pos array) =
  object (_)
    inherit player a_name a_pen
    method is_human = true
    (* implemented in gui *)
    method cpu_move map = (penguins.(0), penguins.(0))
    method play = ()
  end

class iaStandardPlayer (a_name:string) (a_pen:Move.pos array) =
  object (_)
    inherit player a_name a_pen
    method is_human = false
    (* implemented in game_state.cpu_move *)
    (* TODO : call game_state.cpu_move here with array of players & map *)
    method cpu_move map =
      let penguins_array= penguins in
      let penguins_list = ref [] in
      for i=0 to (Array.length penguins_array -1) do
        penguins_list:= (penguins_array.(i))::(!penguins_list)
      done;
      let (a,b)=(Computer.ai map !penguins_list) in
      let n = Move.move_n map a b in
      (a, n)
    method play = ()
  end

class iaFacilePlayer (a_name:string) (a_pen:Move.pos array) =
object (_)
  inherit player a_name a_pen
  method is_human = false
  (* implemented in game_state.cpu_move2 *)
  (* TODO : call game_state.cpu_move2 here with array of players & map *)
  method cpu_move map =
      let penguins_array= penguins in
      let penguins_list = ref [] in
      for i=0 to (Array.length penguins_array -1) do
        penguins_list:= (penguins_array.(i))::(!penguins_list)
      done;
      let (a,b)=(Computer.ai2 map !penguins_list) in
      let n = Move.move_n map a b in
      (a, n)
  method play = ()
end


class iaRandomPlayer (a_name:string) (a_pen:Move.pos array) =
object (_)
  inherit player a_name a_pen
  method is_human = false
  (* implemented in game_state.cpu_move2 *)
  (* TODO : call game_state.cpu_move2 here with array of players & map *)
  method cpu_move map =
      let penguins_array= penguins in
      let penguins_list = ref [] in
      for i=0 to (Array.length penguins_array -1) do
        penguins_list:= (penguins_array.(i))::(!penguins_list)
      done;
      let (a,b)=(Computer.ai3 map !penguins_list) in
      let n = Move.move_n map a b in
      (a, n)
  method play = ()
end
